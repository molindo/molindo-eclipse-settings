# README #

This repository contains Eclipse settings suitable for use as a git submodule in other projects.

## Simple project ##

```
#!shell
# delete existing settings
git rm -r .settings/
git commit -m "deleted eclipse settings"
# add .settings
git submodule add -b ${branch} git@bitbucket.org:molindo/molindo-eclipse-settings.git .settings
git commit -m "added molindo-eclipse-settings"
```
## Multi-module project ##

```
#!shell
# delete existing settings
find . -name pom.xml -type f -execdir git rm -r .settings/ \;
git commit -m "deleted eclipse settings"
# add .settings
find . -name pom.xml -type f -execdir git submodule add -b ${branch} git@bitbucket.org:molindo/molindo-eclipse-settings.git .settings \;
git commit -m "added molindo-eclipse-settings"
```

## Branches ##
* `java8`
* `java17`
